export function map(elements, cb) {
    if (Array.isArray(elements) === false) {
        return "Not An Array";
    }
    for (let i = 0; i < elements.length; i++) {
        elements[i] = cb(elements[i]);
    }
    return elements;
}