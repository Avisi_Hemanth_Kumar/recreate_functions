export function reduce(elements, cb, startingValue = elements[0]) {
    if (Array.isArray(elements) === false) {
        return "Not An Array";
    }

    for (let i = 1; i < elements.length; i++) {
        startingValue = cb(startingValue, elements[i]);
    }
    return startingValue;
}