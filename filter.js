export function filter(elements, cb) {

    const fElement = [];

    if (Array.isArray(elements) === false) {
        return "Not An Array";
    }

    for (let i = 0; i < elements.length; i++) {
        if (cb(elements[i]) === true) {
            fElement.push(elements[i]);
        }
    }

    return fElement;
}