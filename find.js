export function find(elements, cb) {

    if (Array.isArray(elements) === false) {
        return "Not An Array";
    }
    for (let i = 0; i < elements.length; i++) {
        if (cb(elements[i]) === true) {
            return elements[i];
        }
    }
    return undefined;
}