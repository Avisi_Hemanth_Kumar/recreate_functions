import {
    filter
} from '/home/hemanth/MountBlue/RecreateFunctions/filter.js'

function testFilter(actual, expected) {
    if (JSON.stringify(actual) === JSON.stringify(expected)) {
        return "PASS";
    }
    return "FAIL";
}

const elements = [1, 2, 3, 4, 5, 5];

const actual = filter(elements, function (value) {
    return value > 3;
});

const expected = elements.filter(function (value) {
    return value > 3;
});

console.log(actual);
console.log(expected);
console.log(testFilter(actual, expected));