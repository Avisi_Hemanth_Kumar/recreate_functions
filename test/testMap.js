import {
    map
} from '/home/hemanth/MountBlue/RecreateFunctions/map.js';

function testMap(actual, expected) {
    if (JSON.stringify(actual) === JSON.stringify(expected)) {
        return "PASS";
    }
    return "FAIL";
}

const elements1 = [1, 2, 3, 4, 5, 5];
const elements2 = [1, 2, 3, 4, 5, 5];

const actual = map(elements1, function (value) {
    return value * 5;
});

const expected = elements2.map(function (value) {
    return value * 5;
});

console.log(actual);
console.log(expected);
console.log(testMap(actual, expected));