import {
    flatten
} from '/home/hemanth/MountBlue/RecreateFunctions/flatten.js';

function testFlatten(actual, expected) {
    if (JSON.stringify(actual) === JSON.stringify(expected)) {
        return "PASS";
    }
    return "FAIL";
}

const elements = [1, [2],
    [
        [3]
    ],
    [
        [
            [4]
        ]
    ],
    [
        [3]
    ],
    [
        [3]
    ],
    [
        [3]
    ],
    [
        [
            [4]
        ]
    ],
    [
        [
            [4]
        ]
    ],
    [
        [
            [4]
        ]
    ]
];

const actual = flatten(elements);

const expected = elements.flat(Infinity);

console.log(actual);
console.log(expected);
console.log(testFlatten(actual, expected));