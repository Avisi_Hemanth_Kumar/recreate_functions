import {
    reduce
} from '/home/hemanth/MountBlue/RecreateFunctions/reduce.js'

function testReduce(actual, expected) {
    if (JSON.stringify(actual) === JSON.stringify(expected)) {
        return "PASS";
    }
    return "FAIL";
}

const elements = [1, 2, 3, 4, 5, 5];

const actual = reduce(elements, function (accumulator, operand) {
    return Math.max(accumulator, operand);
});

const expected = elements.reduce(function (accumulator, operand) {
    return Math.max(accumulator, operand);
});

console.log(actual);
console.log(expected);
console.log(testReduce(actual, expected));