import {
    each
} from '/home/hemanth/MountBlue/RecreateFunctions/each.js'

function testEach(actual, expected) {
    if (JSON.stringify(actual) === JSON.stringify(expected)) {
        return "PASS";
    }
    return "FAIL";
}

const elements = [1, 2, 3, 4, 5, 5];

const actual = each(elements, function (value, index) {
    console.log('Value : ' + value + ' Index : ' + index);
});

console.log("\n");

const expected = elements.forEach(function (value, index) {
    console.log('Value : ' + value + ' Index : ' + index);
});