import {
    find
} from '/home/hemanth/MountBlue/RecreateFunctions/find.js'

function testFind(actual, expected) {
    if (JSON.stringify(actual) === JSON.stringify(expected)) {
        return "PASS";
    }
    return "FAIL";
}

const elements = [5, 12, 8, 130, 44];

const actual = find(elements, function (value) {
    return value > 20;
});

const expected = elements.find(function (value) {
    return value > 20;
});

console.log(actual);
console.log(expected);
console.log(testFind(actual, expected));