export function each(elements, cb) {

    if (Array.isArray(elements) === false) {
        return "Not An Array";
    }

    for (let i = 0; i < elements.length; i++) {
        cb(elements[i], i);
    }

}