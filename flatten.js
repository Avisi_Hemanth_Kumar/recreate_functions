export function flatten(elements) {

    if (Array.isArray(elements) === false) {
        return "Not An Array";
    }

    const newArr = [];

    function checkArray(elements) {
        for (let i = 0; i < elements.length; i++) {
            if (Array.isArray(elements[i]) === true) {
                checkArray(elements[i]);
            } else {
                newArr.push(elements[i]);
            }
        }
    }

    checkArray(elements);

    return newArr;
}